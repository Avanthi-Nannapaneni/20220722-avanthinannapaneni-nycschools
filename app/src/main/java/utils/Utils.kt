package utils

val DEFAULT_CONNECT_TIMEOUT_IN_MS: Long = 90000
val DEFAULT_WRITE_TIMEOUT_IN_MS: Long = 90000
val DEFAULT_READ_TIMEOUT_IN_MS: Long = 90000
val END_POINT = "https://data.cityofnewyork.us/"

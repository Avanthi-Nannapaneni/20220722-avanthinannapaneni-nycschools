package network

import retrofit2.http.GET
import java.util.*

interface SchoolRestServices {

    @GET("resource/s3k6-pzi2.json")
    fun getListOfSchools(): Observable
}
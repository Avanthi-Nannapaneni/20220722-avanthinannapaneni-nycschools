package com.example.a20220722_avanthinannapaneni_nycschools

class SchoolContract {

    interface View {
        fun onInitVIew()
    }

    interface Model {

    }

    interface Presenter {
        fun init()
        fun getSchoolList()
    }
}


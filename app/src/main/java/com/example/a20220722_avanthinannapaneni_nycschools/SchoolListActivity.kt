package com.example.a20220722_avanthinannapaneni_nycschools

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchoolListActivity : AppCompatActivity(), SchoolContract.View {
    private lateinit var activity: Activity
    private lateinit var model: SchoolContract.Model
    private lateinit var presenter: SchoolContract.Presenter
    private lateinit var loaderView: ProgressBar
    private lateinit var schoolListRecyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_list)
        activity = this
        model = SchoolModelImpl(activity)
        presenter = SchoolPresenterImpl(this, model, Schedulers.io(), AndroidSchedulers.mainThread())
        presenter.init()
    }

    override fun onInitVIew() {
        loaderView = findViewById(R.id.loaderBar)
        schoolListRecyclerView = findViewById(R.id.schoolList)
        presenter.getSchoolList()
    }
}
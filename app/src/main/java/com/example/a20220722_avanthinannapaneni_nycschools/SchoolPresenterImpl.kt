package com.example.a20220722_avanthinannapaneni_nycschools

import io.reactivex.Scheduler

class SchoolPresenterImpl(
    view: SchoolContract.View,
    model: SchoolContract.Model,
    processThread: Scheduler,
    mainThread: Scheduler
) : SchoolContract.Presenter {

    private var view: SchoolContract.View = view
    private var model: SchoolContract.Model = model
    private var processThread: Scheduler = processThread
    private var mainThread: Scheduler = mainThread


    override fun init() {
        view.onInitVIew()
    }

    override fun getSchoolList() {

    }
}